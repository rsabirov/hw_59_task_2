import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Memes from './components/Memes/Memes';

class App extends Component {
  render() {
    return (
      <div className="App">
          <Memes />
      </div>
    );
  }
}

export default App;
