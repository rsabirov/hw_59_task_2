import React, {Component} from 'react';

class Add extends Component {
    render() {
        return (
            <div className="container">
                <button type="button" className="btn btn-primary" onClick={() => this.props.fetch()}>Add Memes</button>
            </div>
        );
    }
}
export default Add;
