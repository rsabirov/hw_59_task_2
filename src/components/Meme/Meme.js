import React, {Component} from 'react';

class Meme extends Component {
    render() {
        return (
            <div className="col-3">
                <div className="card">
                    <img className="card-img-top" src={this.props.iconUrl} alt="" />
                    <div className="card-body">
                        <h5 className="card-title">{this.props.nn}</h5>
                        <h6 className="card-subtitle mb-2 text-muted">{this.props.category}</h6>
                        <p className="card-text">{this.props.text}</p>
                    </div>
                </div>
            </div>
        );
    }
}
export default Meme;
