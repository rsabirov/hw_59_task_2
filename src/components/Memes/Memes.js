import React, { Component, Fragment } from 'react';
import Meme from '../Meme/Meme';
import Add from '../Add/Add';

class Memes extends Component {

  state = {
    memes: []
  };

    componentDidMount() {
        this.fetchMeme();
    }

    fetchMeme = () => {

        const M_URL = 'https://api.chucknorris.io/jokes/random';

        fetch(M_URL).then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Something went wrong with network request');
        }).then(meme => {
            const memes = this.state.memes;
            memes.push(meme);
            this.setState({memes});

        }).catch(error => {
            console.log(error);
        })

    };



  render() {
      console.log(this.state.memes);
      return (
      <Fragment>
        <section className="add">
          <Add fetch={this.fetchMeme} />
        </section>
        <section className="Memes">
          <div className="container">
              <div className="row justify-content-center">
                  {this.state.memes.map((meme, index) => (
                      <Meme nn={meme.nn}
                            key={index}
                            text={meme.value}
                            id={meme.id}
                            category={meme.category}
                            iconUrl={meme.iconUrl}
                      />
                  ))}
              </div>
          </div>
        </section>
      </Fragment>
    );
  }
}

export default Memes;